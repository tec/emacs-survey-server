#!/usr/bin/env sh
if ! dpkg -s caddy > /dev/null; then
    printf "\e[1;34mInstalling Caddy\e[m\n"
    # Taken from <https://caddyserver.com/docs/install#debian-ubuntu-raspbian>
    sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
    sudo apt update
    sudo apt install caddy
fi

JULIA_VERSION="1.8.2"

if ! command -v julia > /dev/null || [ ! "$(julia --version)" = "julia version $JULIA_VERSION" ]; then
    printf "\e[1;34mInstalling Julia\e[m\n"
    cd /tmp || exit 2
    # Following <https://julialang.org/downloads/platform/>
    wget "https://julialang-s3.julialang.org/bin/linux/x64/${JULIA_VERSION%.*}/julia-$JULIA_VERSION-linux-x86_64.tar.gz"
    mkdir -p "/opt/julia/"
    tar zxf "julia-$JULIA_VERSION-linux-x86_64.tar.gz" --directory=/opt/julia
    ln -sf "/opt/julia/julia-$JULIA_VERSION/bin/julia" /usr/bin/julia
fi

SETUPDIR=$(dirname "$(readlink --canonicalize-existing "$0")")

cp -f "$SETUPDIR/Caddyfile" "/etc/caddy/Caddyfile"

if ! id genie >/dev/null; then
    groupadd -f genie
    useradd -r -g genie -m -d /var/lib/genie genie
fi

if [ -d /opt/emacs-survey ]; then
    cd /opt/emacs-survey || exit 2
    printf "\e[1;34mUpdating emacs-survey\e[m\n"
    sudo -u genie git pull origin main --ff-only
else
    mkdir -p /opt/emacs-survey
    chown genie:genie /opt/emacs-survey
    printf "\e[1;34mCloning emacs-survey\e[m\n"
    sudo -u genie git clone https://git.tecosaur.net/tec/emacs-survey /opt/emacs-survey
fi

printf "\e[34mInstantiating emacs-survey\e[m\n"
sudo -u genie julia --project=/opt/emacs-survey -e 'using Pkg; Pkg.instantiate()'

if [ ! -e "/opt/emacs-survey/config/secrets.jl" ]; then
   printf "\e[34mCreating genie secrets token\e[m\n"
   cd /opt/emacs-survey
   sudo -u genie julia --project=/opt/emacs-survey -e 'using Genie; Genie.Generator.write_secrets_file()'
fi

cp -f "$SETUPDIR/genie-app.service" /etc/systemd/system/
systemctl daemon-reload
